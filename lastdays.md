## Last days instructions.

1. Collect a treat from the treat pile 
2. Have a look at http://arcade.academy/sample_games.html and download and play at least one game.
3. Have a look at https://wiki.python.org/moin/PythonGames and find one game to play at home.  Explain to Keith (or Tom or any other older type person in the classroom), and grab a treat from the pile.
4. After 9:45 start looking for a branch with different instructions