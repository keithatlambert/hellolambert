## What did we do this year?

1. Powerpoint on python https://www.python.org/downloads/
2. Pycharm https://www.jetbrains.com/pycharm/download/#section=windows
3. We spent a lot of time with Arcade Acadamy http://arcade.academy/
4. We looked at read the docs for using python and arcade acadamy at read the docs https://arcade-book.readthedocs.io/en/latest/chapters/05_print_statement/print_statement.html
5. We made snowman, learned to sort and search like a computer and make some guessing games.
6. Today the first job is to download this repository at https://bitbucket.org/keithatlambert/hellolambert


## How might we do this?

1. Put your USB thumbdrive into your computer.
2. Open git bash on your computers
3. cd to the directory.  Keith does not have a computer set up the same as the school computers so might need to provide instructions on the day.
4. as per the headings on the page that has this, "git clone https://keithatlambert@bitbucket.org/keithatlambert/hellolambert.git"
5. have a look at the folder on your disk.  Have a look at the URL https://bitbucket.org/keithatlambert and compare the contents of the downloaded folder and this URL.
6. find a file called lastdays.md and follow its instructions.
